package com.example.bianca.farmasolidria

import java.io.Serializable

data class Medicamento(
    val produto: String,
    val apresentacao: String,
    val tipo: String,
    val concentracao: String,
    val conteudo: String,
    val ean1: String
  //  val quantidade: String
): Serializable