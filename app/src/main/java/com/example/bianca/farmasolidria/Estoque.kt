package com.example.bianca.farmasolidria

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_estoque.*
import kotlinx.android.synthetic.main.tipo_item.*

class Estoque : AppCompatActivity() {

    companion object {
        public const val MEDICAMENTO: String = "Medicamento" //para putExtra entre activities
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estoque)

        val limiteNM = 2 // limite entre normal e muito; precisa ser >limitePN
        val limitePN = 1 // limite entre pouco e normal; precisa ser >0 e <limiteNM
        // Fica assim: if (qtd > limiteNM) "MUITO" else if (qtd > limitePN) "NORMAL" else if (qtd > 0) "POUCO" else "NADA"

        val rootRef = FirebaseFirestore.getInstance()
        val medicamentosRef = rootRef.collection("medicamentos")
        val estoqueRef = rootRef.collection("estoque")

        val medicamento: Medicamento ? = intent.getSerializableExtra(MEDICAMENTO) as Medicamento


        if (medicamento != null){
            tvQntd.text = getString(R.string.carregando)
            medicamentosRef
                .whereEqualTo("produto", medicamento.produto)
                .get()
                .addOnSuccessListener {medDocuments ->
                    var nMeds = medDocuments.size()
                    if (nMeds == 0) tvQntd.text = getString(R.string.houve_falha)
                    var nNoEstoque = 0
                    for (medDocument in medDocuments) {
                        estoqueRef
                            .whereEqualTo("medicamento", medDocument.reference)
                            .get()
                            .addOnSuccessListener { estoqueDocuments ->
                                for (estoqueDocument in estoqueDocuments) {
                                    val qtd = estoqueDocument.data.get("quantidade")
                                    nNoEstoque += qtd.toString().toInt()
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.w("estoque não lido", "Error gettint documents: ", exception)
                            }
                            .addOnCompleteListener {
                                nMeds--
                                if (nMeds == 0) {
                                    tvQntd.text =
                                        if (nNoEstoque > limiteNM)
                                            getString(R.string.muita_qtd)
                                        else if (nNoEstoque > limitePN)
                                            getString(R.string.qtd_suficiente)
                                        else if (nNoEstoque > 0)
                                            getString(R.string.pouca_qtd)
                                        else
                                            getString(R.string.em_falta)
                                }
                            }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w("doc não lido", "Error getting documents: ", exception)
                    tvQntd.text = getString(R.string.houve_falha)
                }

            carregaDados(medicamento)
        }
    }

    private fun carregaDados(medicamento: Medicamento) {
        tvProduto.setText(medicamento.produto)
        tvConcentracao.setText(medicamento.concentracao)
        //tvQntd.setText(medicamento.quantidade)


    }
}
