package com.example.bianca.farmasolidria

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.algolia.search.saas.*
import com.example.bianca.farmasolidria.Estoque.Companion.MEDICAMENTO
import kotlinx.android.synthetic.main.activity_lista_medicamentos.*
import org.json.JSONException
import java.util.ArrayList

val applicationID = "K2GYD1T63R"
val apikey = "1af9965a2a7be67290ec8d188a6c0210"

class ListaMedicamentosActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_medicamentos)

        val client = Client(applicationID, apikey)
        val index = client.getIndex("med_search")

        val that = this

        edtBusca.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val query =
                    Query(edtBusca.text.toString()).setAttributesToRetrieve("produto").setHitsPerPage(20).setDistinct(1)
                pbLoading.visibility = View.VISIBLE
                index.searchAsync(query) { content, error ->
                    try {
                        val hits = content!!.getJSONArray("hits")
                        val list = ArrayList<Medicamento>()
                        for (i in 0 until hits.length()) {
                            val json = hits.getJSONObject(i)
                            val produto = json.getString("produto")

                            val highlight = json.getJSONObject("_highlightResult")
                            val apresentacao = highlight.getJSONObject("apresentacao").getString("value")
                            val tipo = highlight.getJSONObject("tipo").getString("value")
                            val concentracao = highlight.getJSONObject("concentracao").getString("value")
                            val conteudo = highlight.getJSONObject("conteudo").getString("value")
                            val ean1 = highlight.getJSONObject("ean1").getString("value")
                            // val quantidade = highlight.getJSONObject("quantidade").getString("value")

                            val medicamento = Medicamento(produto, apresentacao, tipo, concentracao, conteudo, ean1)

                            list.add(medicamento)
                        }


                        val adapter = MedicamentosAdapter(that, list) { medicamento ->
                            println(medicamento.produto + " " + medicamento.ean1)
                            val exibeEstoque = Intent(that, Estoque::class.java)
                            exibeEstoque.putExtra(MEDICAMENTO, medicamento)
                            that.startActivity(exibeEstoque)
                        }

                        val lM = LinearLayoutManager(that)
                        rvMedicamentos.layoutManager = lM
                        rvMedicamentos.adapter = adapter
                        val dividerItemDecoration = DividerItemDecoration(that, lM.orientation)
                        rvMedicamentos.addItemDecoration(dividerItemDecoration)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    pbLoading.visibility = View.GONE
                }

            }
        })

    }


    override fun onResume() {
        super.onResume()
    }
}
