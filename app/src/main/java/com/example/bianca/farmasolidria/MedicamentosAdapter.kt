package com.example.bianca.farmasolidria

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.medicamentos_item_lista.view.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.tipo_item.view.*

class MedicamentosAdapter(val context: Context,
                          val medicamentos: List<Medicamento>,
                          val clique: ((medicamento:Medicamento)->Unit)) : GroupAdapter<ViewHolder>() {



    /*recebe lista desordenada de medicamentos
    init {
        //retorna um HashMap com Ma lista agrupada por tipo (genérico, etc)
        val medicamentosGrouped = medicamentos.groupBy { it.tipo }
        medicamentosGrouped.keys.forEach { tipo ->
            //adiciona tipo na lista
            add(TypeItem(tipo))
            //  a cada tipo adicionado, adiciona todos os produtos relacionados a esse tipo
            medicamentosGrouped.get(tipo)?.forEach { medicamento ->
                add(MedicamentoItem(medicamento))
            }

        }
    }*/

    init {
        //retorna um HashMap com a lista agrupada por tipo (genérico, etc)
        medicamentos.forEach { medicamento->
                add(MedicamentoItem(medicamento))
        }

    }

    inner class MedicamentoItem(val medicamento: Medicamento) : Item() {

        //itens a serem exibidos em cada item do Groupie
        override fun bind(viewHolder: ViewHolder, position: Int) {
            viewHolder.containerView.tvNome.text = medicamento.produto
            viewHolder.containerView.tvDosagem.text = medicamento.concentracao

            viewHolder.containerView.setOnClickListener{
                clique.invoke(medicamento)

            }


        }

        override fun getLayout(): Int = R.layout.medicamentos_item_lista

    }

    /*inner class TypeItem(val produto: String) : Item() {
        override fun bind(viewHolder: ViewHolder, position: Int) {
            viewHolder.containerView.tvTipo.text = produto
        }

        override fun getLayout(): Int = R.layout.tipo_item
    }*/
}